// import * as Papa from "./external-module/papaparse.js"; */
/* import _ from "./node_modules/lodash/lodash.js"; */


var data_list = document.getElementById("ice-cream-flavors"); 


function add_option(option_text){
var option = document.createElement('option');
option.text = option_text;
data_list.appendChild(option);
  
}


var configuration = {	delimiter: ",",	// auto-detect
	newline: "",	// auto-detect
	quoteChar: '"',
	escapeChar: '"',
	header: true,
	transformHeader: undefined,
	dynamicTyping: false,
	preview: 0,
	encoding: "",
	worker: false,
	comments: false,
	step: undefined,
        complete: papa_complete,
	error: undefined,
	download: true,
	downloadRequestHeaders: undefined,
	downloadRequestBody: undefined,
	skipEmptyLines: false,
	chunk: undefined,
	chunkSize: undefined,
	fastMode: undefined,
	beforeFirstChunk: undefined,
	withCredentials: undefined,
	transform: undefined,
	delimitersToGuess: [',', '\t', '|', ';'],
	skipFirstNLines: 0
		    };


// const response = fetch('./city_coordinates.csv')
//       .then(response => response.text())
//       .then(v => Papa.parse(v))
//       .catch(err => console.log(err));


var source_data;

function papa_complete(results) {
    source_data = results.data;
    console.log(source_data);
    _.forEach(source_data, function(value) {
    add_option(value.city);
});

}




function get_selected_town(town_name){
   return _.find(source_data, function(o) { return o.city == town_name; });
}


function get_lon(town_name){
    return get_selected_town(town_name).longitude  ;
}

function get_lat(town_name){
    return get_selected_town(town_name).latitude;
}

function get_location(town_name){
    
    console.log(get_lat(selected_city) + ", " + get_lon(selected_city));
}
//console.log(get_lat("Seoul"));	// 

Papa.parse("./city_coordinates.csv",configuration);


async function get_weather_forecast() {
    let results;
    var selected_city = document.getElementById("ice-cream-choice").value;
    let lat = get_lat(selected_city);
    let lon = get_lon(selected_city);
    let api_request = "https://www.7timer.info/bin/civillight.php?lon=" + lon + "&lat=" + lat + "&ac=0&unit=metric&output=json&tzshift=0";
    const post = await fetch(api_request);
    results = await post.json();
    return results ;
 }

function format_date(text){
    text = text.toString()
    let year = text.match("\\d{4}");
    let month = text.match("(?<=\\d{4})\\d{2}")
    let day = text.match("(?<=\\d{6})\\d{2}")
    return year + "-" + month + "-" + day

}

function create_date_display(obj){
    let p =  document.createElement("p");
    let formatted_date = format_date(obj.date)
    p.innerHTML = formatted_date
    return p;
}

function show_icon(weather){
    let path = "images/"+ weather+".png "
    return '<img src=' + path +" alt=" + "weather>"

}

function create_weather_display(obj){
    var p =  document.createElement("p");
    p.innerHTML = show_icon(obj.weather);
    return p;
}
function create_temperature_display(obj){
    var p = document.createElement("p");
    p.innerHTML = "min: " + obj.temp2m.min + " max: " + obj.temp2m.max
    return p
}

function create_weather_div (obj) {
    var weather_info = document.getElementById("weather-info");
    var weather_display_container = document.createElement("div");
    weather_display_container.className = "column";
    var date_display  = create_date_display(obj);
    var weather_display = create_weather_display(obj);
    var temperature_display = create_temperature_display(obj);
    weather_display_container.appendChild(date_display);
    weather_display_container.appendChild(weather_display);
    weather_display_container.appendChild(temperature_display);
    weather_info.appendChild(weather_display_container);
}

async function display_weather(promise){
    promise.then(function(x){
	x.dataseries.map(function(x) {create_weather_div(x);});
    });
     //promise.then(function(x)

}

function clear_view(){
    var  part = document.getElementById("weather-info")
    part.innerHTML = ""
}
function view_weather(){
    clear_view()
    let results =   get_weather_forecast();
    display_weather(results);
}


async function get_test(){
 //    
 return {dataseries : [{date:1, weather: "cloudy"}]} 
} 

// have a test object which is similar to the object returned by the api
// this will avoid the need to call the api during testing phase.
var test_data = get_test()

// async function get_date(promise){
//     promise.then(function(x) x)
// }
// }
